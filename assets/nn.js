/*
Basic draw-stuff from:
http://bencentra.com/code/2014/12/05/html5-canvas-touch-events.html
https://github.com/bencentra/canvas/blob/master/signature/
*/

(() => {
    const inputCard = document.getElementById("input-card");
    const nnCertaintiesCvs = document.getElementById("nn-certainties");
    const nnHighestGuessOutput = document.getElementById(
        "nn-highestGuess_output"
    );
    const nnHighestGuessCertainty = document.getElementById(
        "nn-highestGuess_certainty"
    );
    const drawAbove = document.getElementById("draw-above");

    submitTimeout = 0;
    const submitTimeoutText = document.getElementById("submitTimeoutText");

    const nnCertainties = new Chart(nnCertaintiesCvs, {
        type: "bar",
        data: {
            labels: Array.from({ length: 10 }, (e, i) => i.toString()),
            datasets: [
                {
                    label: "Certainty",
                    data: [],
                    backgroundColor: Array.from(
                        { length: 10 },
                        (e, i) => "rgba(0, 0, 0, 0.5)"
                    )
                }
            ]
        },
        options: {
            animation: {
                easing: "easeOutElastic"
            },
            tooltips: {
                intersect: false,
                mode: "index"
            },
            legend: {
                display: false,
                onClick: e => {
                    e.preventDefault();
                    return false;
                }
            }
        }
    });

    // Get a regular interval for drawing to the screen
    window.requestAnimFrame = (function(callback) {
        return (
            window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimaitonFrame ||
            function(callback) {
                window.setTimeout(callback, 1000 / 60);
            }
        );
    })();

    // Set up the canvas
    let cvs = document.getElementById("user-input");
    let ctx = cvs.getContext("2d");
    let cvs2 = document.getElementById("nn-input");
    let ctx2 = cvs2.getContext("2d");
    ctx.lineWidth = 24;
    ctx2.imageSmoothingEnabled = false;
    let cvsCleared = true;

    const randomizeLineWidth = () => {
        ctx.lineWidth = Math.floor(Math.random() * (28 - 20) + 20);
    };

    const pixelateCanvas = () => {
        ctx2.clearRect(0, 0, 28, 28);
        ctx2.save();
        ctx2.scale(28 / 448, 28 / 448);
        ctx2.drawImage(cvs, 448 / 14, 448 / 14);
        ctx2.restore();
    };

    const unPixelateCanvas = () => {
        ctx.clearRect(0, 0, 384, 384);
        ctx.save();
        ctx.scale(448 / 28, 448 / 28);
        ctx.drawImage(cvs2, -2, -2);
        ctx.restore();
    };

    // Set up the UI
    let clearBtn = document.getElementById("input-clear");
    let submitBtn = document.getElementById("nn-run");
    clearBtn.addEventListener(
        "click",
        e => {
            clearCanvas();
        },
        false
    );
    submitBtn.addEventListener(
        "click",
        event => {
            if (submitTimeout > 0) return;

            let data = "";

            if (cvsCleared) {
                randImg = randMnistTestImg();

                console.log(randImg);

                let imgData = ctx2.createImageData(28, 28);
                imgData.data.set(
                    randImg.data.flatMap(e => {
                        return [0, 0, 0, e];
                    })
                );

                data = randImg.data.toString();

                ctx2.putImageData(imgData, 0, 0);
                unPixelateCanvas();
                drewToCanvas();
                pixelateCanvas();
            } else {
                pixelateCanvas();

                data = ctx2
                    .getImageData(0, 0, 28, 28)
                    .data.filter((e, i) => {
                        return i % 4 === 3;
                    })
                    .toString();
            }
            console.log("Data getting passed to NN >>", data);

            submitTimeout = 15 * 1000;

            submitBtn.disabled = true;
            submitBtn.classList.add("loading");
            submitBtn.classList.add("waiting");

            fetch("/nn-run/" + data).then(res => {
                res.json().then(body => {
                    console.log(body);

                    if (body.length === 12) {
                        let highestGuess = -1;
                        const certainties = body
                            .map(e => {
                                const m = e.match(
                                    /(?:\d: |>> )((?:.*?)|(?:\d))(?:\%|$)/
                                );
                                return m ? Number(m[1]) : null;
                            })
                            .filter((e, i) => {
                                if (i === 11) {
                                    highestGuess = e;
                                    return false;
                                }
                                return e !== null;
                            });

                        console.log(highestGuess, certainties);

                        nnHighestGuessOutput.innerText = highestGuess;
                        nnHighestGuessCertainty.innerText =
                            certainties[highestGuess];
                        $("#nn-highestGuess").toast("show");

                        nnCertainties.data.datasets[0].data = certainties;
                        nnCertainties.data.datasets[0].backgroundColor = Array.from(
                            { length: 10 },
                            (e, i) =>
                                i === highestGuess
                                    ? "rgba(255, 0, 0, 0.75)"
                                    : "rgba(0, 0, 0, 0.5)"
                        );

                        // inputCard.classList.add("hidden");
                        // cvs.classList.add("d-none");
                        // cvs2.classList.add("d-none");
                        nnCertaintiesCvs.classList.remove("d-none");
                        submitBtn.classList.remove("loading");

                        nnCertainties.update();

                        if (submitTimeout > 5 * 1000) {
                            submitTimeout = 5 * 1000;
                        }
                    }
                });
            });
        },
        false
    );

    // Set up mouse events for drawing
    let drawing = false;
    let mousePos = { x: 0, y: 0 };
    let lastPos = mousePos;
    // let mouseLeft = false;
    cvs.addEventListener(
        "mousedown",
        e => {
            drawing = true;
            lastPos = getMousePos(cvs, e);
        },
        false
    );
    cvs.addEventListener(
        "mouseup",
        e => {
            drawing = false;
            // mouseLeft = false;
        },
        false
    );
    cvs.addEventListener(
        "mouseleave",
        e => {
            // if (drawing) mouseLeft = true;
            drawing = false;
        },
        false
    );
    // cvs.addEventListener("mouseenter", e => {
    //     if (mouseLeft) {
    //         drawing = true;
    //         lastPos = getMousePos(cvs, e);
    //     }
    // });
    cvs.addEventListener(
        "mousemove",
        e => {
            mousePos = getMousePos(cvs, e);
        },
        false
    );

    // Set up touch events for mobile, etc
    cvs.addEventListener(
        "touchstart",
        e => {
            mousePos = getTouchPos(cvs, e);
            let touch = e.touches[0];
            let mouseEvent = new MouseEvent("mousedown", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            cvs.dispatchEvent(mouseEvent);
        },
        false
    );
    cvs.addEventListener(
        "touchend",
        e => {
            let mouseEvent = new MouseEvent("mouseup", {});
            cvs.dispatchEvent(mouseEvent);
        },
        false
    );
    cvs.addEventListener(
        "touchmove",
        e => {
            let touch = e.touches[0];
            let mouseEvent = new MouseEvent("mousemove", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            cvs.dispatchEvent(mouseEvent);
        },
        { passive: false }
    );

    // Prevent scrolling when touching the canvas
    const preventScroll = e => {
        if (e.target === cvs) {
            e.preventDefault();
            return false;
        }
    };
    document.body.addEventListener("touchstart", preventScroll, false);
    document.body.addEventListener("touchend", preventScroll, false);
    document.body.addEventListener("touchmove", preventScroll, {
        capture: false,
        passive: false
    });

    // Get the position of the mouse relative to the canvas
    const getMousePos = (cvsDom, mouseEvent) => {
        let rect = cvsDom.getBoundingClientRect();
        return {
            x: mouseEvent.clientX - rect.left,
            y: mouseEvent.clientY - rect.top
        };
    };

    // Get the position of a touch relative to the canvas
    const getTouchPos = (cvsDom, touchEvent) => {
        let rect = cvsDom.getBoundingClientRect();
        return {
            x: touchEvent.touches[0].clientX - rect.left,
            y: touchEvent.touches[0].clientY - rect.top
        };
    };

    // Draw to the canvas
    const renderCanvas = () => {
        if (drawing) {
            randomizeLineWidth();
            const radius = ctx.lineWidth / 1.25;

            ctx.lineCap = "round";
            ctx.strokeStyle = "#010101F6";
            ctx.lineWidth /= 1;
            ctx.beginPath();
            ctx.moveTo(lastPos.x, lastPos.y);
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.stroke();
            ctx.closePath();

            ctx.beginPath();
            ctx.moveTo(lastPos.x, lastPos.y);
            ctx.fillStyle = "#22222206";
            ctx.arc(lastPos.x, lastPos.y, radius / 0.6, 0, Math.PI * 2);
            ctx.fill();
            ctx.closePath();

            ctx.beginPath();
            ctx.moveTo(lastPos.x, lastPos.y);
            ctx.fillStyle = "#1010100F";
            ctx.arc(lastPos.x, lastPos.y, radius / 0.8, 0, Math.PI * 2);
            ctx.fill();
            ctx.closePath();

            ctx.beginPath();
            ctx.moveTo(lastPos.x, lastPos.y);
            ctx.fillStyle = "#1010106F";
            ctx.arc(lastPos.x, lastPos.y, radius / 0.9, 0, Math.PI * 2);
            ctx.fill();
            ctx.closePath();

            ctx.beginPath();
            ctx.moveTo(lastPos.x, lastPos.y);
            ctx.fillStyle = "#01010160";
            ctx.arc(lastPos.x, lastPos.y, radius / 1, 0, Math.PI * 2);
            ctx.fill();
            ctx.closePath();

            ctx.beginPath();
            ctx.moveTo(mousePos.x, mousePos.y);
            ctx.fillStyle = "#000000";
            ctx.arc(mousePos.x, mousePos.y, radius / 1.25, 0, Math.PI * 2);
            ctx.fill();
            ctx.closePath();

            lastPos = mousePos;

            pixelateCanvas();
            drewToCanvas();
        }
    };

    const drewToCanvas = () => {
        cvsCleared = false;
        clearBtn.classList.remove("d-none");
        if (submitTimeout <= 0) submitBtn.disabled = false;
        submitBtn.classList.remove("cleared");
        drawAbove.classList.remove("cleared");
    };

    const clearCanvas = () => {
        // cvs.width = cvs.width;
        ctx.beginPath();
        ctx.clearRect(0, 0, cvs.width, cvs.height);

        pixelateCanvas();

        // inputCard.classList.remove("hidden");
        // cvs.classList.remove("d-none");
        // cvs2.classList.remove("d-none");
        cvsCleared = true;
        clearBtn.classList.add("d-none");
        // submitBtn.disabled = true;
        submitBtn.classList.add("cleared");
        drawAbove.classList.add("cleared");
        // nnCertaintiesCvs.classList.add("d-none");
    };

    let lastTime = -1;
    // Allow for animation
    (drawLoop = () => {
        if (submitTimeout > 0) {
            const nLastTime = Date.now();
            const passedTime =
                nLastTime - (lastTime === -1 ? nLastTime : lastTime);
            submitTimeout -= passedTime;
            lastTime = nLastTime;
            submitTimeoutText.innerText = parseInt(
                (submitTimeout + 500) / 1000
            );
        } else {
            lastTime = -1;
            submitBtn.classList.remove("waiting");
            // if (!cvsCleared)
            submitBtn.disabled = false;
        }
        requestAnimFrame(drawLoop);
        renderCanvas();
    })();
})();
