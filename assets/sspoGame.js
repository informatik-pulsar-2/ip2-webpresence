(() => {
    const startBtn = document.getElementById("sspo-start");
    const runBtn = document.getElementById("sspo-run");
    const clientInput = document.getElementById("sspo-client-input");
    const clientOutput = document.getElementById("sspo-client-output");

    let clientId = null;
    let getInterval = null;

    startBtn.addEventListener(
        "click",
        e => {
            clientOutput.innerText = "";
            startBtn.disabled = true;
            startBtn.classList.add("loading");
            clientOutput.parentElement.classList.add("d-none");

            fetch("/sspo-start").then(res => {
                res.json().then(body => {
                    console.log(body);

                    if (body.hasOwnProperty("clientId")) {
                        clientId = body.clientId;

                        if (getInterval) {
                            clearInterval(getInterval);
                        }
                        getInterval = setInterval(handleGet, 5000);

                        startBtn.classList.remove("loading");
                        startBtn.classList.add("d-none");
                        runBtn.classList.remove("loading");
                        runBtn.classList.remove("d-none");
                        runBtn.disabled = false;
                        clientInput.disabled = false;
                        clientInput.classList.remove("d-none");
                        clientOutput.parentElement.classList.remove("d-none");
                    }

                    if (body.hasOwnProperty("messages")) {
                        body.messages.map(addMessage);
                    }

                    checkExit(body);
                });
            });
        },
        false
    );

    runBtn.addEventListener(
        "click",
        e => {
            runBtn.classList.add("loading");
            runBtn.disabled = true;
            clientInput.disabled = true;

            const val = clientInput.value.trim();
            if (clientId && val) {
                fetch("/sspo-run/" + clientId + "/" + val).then(res => {
                    res.json().then(body => {
                        console.log(body);

                        if (body.hasOwnProperty("messages")) {
                            body.messages.map(addMessage);
                        }

                        clientInput.value = "";
                        runBtn.classList.remove("loading");
                        runBtn.disabled = false;
                        clientInput.disabled = false;

                        checkExit(body);
                    });
                });
            }
        },
        false
    );

    const addMessage = msg => {
        clientOutput.innerText += msg + "\n";
    };

    const checkExit = body => {
        if (body.hasOwnProperty("exit") && body.exit) {
            clearInterval(getInterval);
            clientId = null;

            startBtn.classList.remove("loading");
            startBtn.classList.remove("d-none");
            startBtn.disabled = false;
            runBtn.classList.remove("loading");
            runBtn.classList.add("d-none");
            runBtn.disabled = true;
            clientInput.disabled = true;
            clientInput.classList.add("d-none");
        }
    };

    const handleGet = () => {
        fetch("/sspo-get/" + clientId).then(res => {
            res.json().then(body => {
                console.log(body);

                if (body.hasOwnProperty("messages")) {
                    body.messages.map(addMessage);
                }

                checkExit(body);
            });
        });
    };
})();
