# Audio Erkennung

> von Sarah & Selin

---

#### Spracherkennung

Die Sprache im Text begann im Jahr 1960 bis 1980. Es wurden Wortkombinationen erstellt und ausgewertet, die ähnlich oder gleich klangen. Die Spracherkennung wird über ein Mikrofon eine menschliche Sprache analysieren, das Ergebnis wird dann mit einer internen Datenbank abgleichen dadurch können einzelne Wörter unterschieden werden. Bevor der Prozess startet wird abgetastet, gefiltert, transformation des Signals in den Frequenzen und das erstellen des Mermalsvektors vorgegangen. Zunächst bei der Abtastung wird das kontinuierliche Signal digitalisiert, also in eine elektronische bearbeitbare Bitfolge zerlegt, um es einfacher weiter verarbeiten zu können. Danach folgt die Filterung bei der die wichtigste Aufgabe ist die Unterscheidung von Umgebungsgeräuschen wie rauschen oder z.B. Motorengeräuschen und Sprache. Dazu werden zum Beispiel die Energie des Signals oder die Nulldurchgangsrate herangezogen. Für die Spracherkennung ist nicht das Zeitsignal, sondern das Signal im Frequenzbereich relevant. Dazu wird es mittels FFT transformiert. Aus dem Resultat, dem Frequenzspektrum, lassen sich die im Signal vorhanden Frequenzanteile ablesen. Zur eigentlichen Spracherkenung wird ein Merkmalsvektor erstellt. Dieser besteht aus voneinander abhängigen oder unabhängigen Merkmalen, die aus dem digitalen Sprachsignalerzeugt werden.

#### Musikerkennung

Bei der Musikerkennung werden die jeweiligen Songs sofort aufgenommen und erkannt, sprich der Name und der Künstler werden angezeigt. Dies funktioniert mit folgenden Musikerkennungsapps: Shazam, Siri oder ähnlichem. Die Anwendung hat eine große Datenbank mit mehr als einer Millionen Lieder, jeder gespeicherte Song besitzt eine eigene Frequenz und schon ein kurzer Teil eines Liedes reicht aus, um das Lied zu erkennen. Verschiedene Erkennungszeichen stellen sich aus verschiedenen Merkmalen zusammen. Zum Beispiel: folgt ein hoher Ton A auf einem tiefen Ton B, stellt das bereits ein Merkmal für einen musikalischen Fingerabdruck dar. Viel solcher Merkmale lassen sich zu einem großen Fingerabdruck bilden. Sobald man mit der Anwendung einen Song aufnimmt, wird nur eine kurze Sequenz mitgehört und an die App Server gesendet, nach wenigen Sekunden wird auf auffällige Merkmale der Sequenzgeprüft und mit der Datenbank verglichen.

#### Google Pixel Buds

Bei den Google Pixel Buds handelt es sich um Kopfhörer die mit einer augenscheinlichen einzigartigen Echtzeit Übersetzung ausgestattet sein soll und 40 Sprachen unterstützt. Sobald man einen bestimmten Button drückt wird das übersetzten der Sprache gestartet. Es nimmt die ausgesprochenen Wörter war und wandelt sie direkt in die gewünschte Sprache um, anders ist es bei Google translator, da tippt man die Wörter schriftlich ein und erhält erst dann ein Ergebnis.
