# Künstliche Intelligenz - Heilung der Welt oder ihr Untergang?

> von Luna

---

Schon seit Jahren ist das Thema KI (Künstliche Intelligenz) oder im englischen AI (artificial intelligence) keine Fiktion mehr. Es hat sich von Science Fiction in die Mitte unserer Gesellschaft projiziert. Jedoch sind die Meinungen sehr unterschiedlich, wo die einen begeistert sind und unerschöpfliche Möglichkeiten sehen, fürchten die anderen, dass das Wissen einer Maschine unsere Vorstellung von Intelligenz übertreffen könnte.

Viele Wissenschaftler warnen davor, dass die Erschaffung einer künstlichen Intelligenz zu gefährlich sei. Jeder Wissenschaftler sollte sich bewusst sein, und die Verantwortung für seine Schöpfung übernehmen und wie es die Zukunft beeinflussen könnte. Die Frage ob man etwas erschaffen könnte und auch sollte, ist eine altbekannte ethische Frage. So warnt der Physiker [Stephen Hawking](https://www.zeit.de/thema/stephen-hawking) ebenso wie die Silicon-Valley-Milliardäre Bill Gates und Elon Musk vor der Gefahr der Künstlichen Intelligenz.

Künstliche Intelligenz ist heute schon weit entwickelt. Bereits im März 2016 wurde ein Programm für das alte Brettspiel GO kreiert, das als das komplexeste Spiel der Welt gilt. Das Programm konnte den Weltranglistenersten der Go-Spieler Lee Sedol dreimal in Folge schlagen. Dies machte das Programm mit Zügen, die von Profis als „Alien“ bezeichnet wurden, weil sie so kreativ und überraschend waren, dass kein Mensch je darauf gekommen wäre. Das Programm AlphaGo, das angewendet wurde, funktioniert mit einem neuronalen Netzwerk, das dem Gehirn ähnlich ist. Die Lösungswege sind bei neuronalen Netzen nicht zugänglich. Niemand weiß wie AlphaGo auf seine Züge gekommen ist. AlphaGo ist ein Programm, das sich selbst weiterentwickelt und somit lernt ein Zeichen für Intelligenz.

Der 78- jährige Wissenschaftler Robert Trappl ist ein begeisterter Fürsprecher der Erschaffung von künstlicher Intelligenz. Bereits 1986 prophezeite Trappl, was heute unermüdlich diskutiert wird: [AI werde körperliche und geistige Arbeit übernehmen](https://www.zeit.de/thema/kuenstliche-intelligenz). Das ist heute bereits unbestritten.

Trappl hat sich unteranderem mit dem Einfluss von Emotionen auf Entscheidungen beschäftigt, denn Forschungen zeigten, dass Menschen mit mehr Emotionen bessere Entscheidungen treffen – weshalb auch intelligente Maschinen Emotionen benötigen würden. 

Viele Menschen sorgen sich um die Zukunft, da die ungeheuren Forschungsgelder für KI in den Händen des Militärs liegt. Allein im Vorjahr wurden laut der Unternehmensberatung [McKinsey](https://www.zeit.de/thema/mckinsey) zwischen 26 und 39 Milliarden Dollar in AI investiert. Es wird spekuliert, dass nach der Evolution von herkömmlichen Waffen, über Atomwaffen nun eine neue Art der Kriegsführung kommen wird. Militärische Forschung ist heute ein großer Teil der KI Forschung, dort gehe es darum, "effektiver Krieg zu führen".

Der optimistische Trappl versucht, seine durch den 2. Weltkrieg geprägte Kindheit mit Hilfe von KIs Kriege zu verhindern. Als Ergebnis erschuf er einen Algorithmus, der vergangene Kriege analysiert, um bei aufkeimenden Konflikten die aussichtsreichste Lösung vorzuschlagen.

Immer wieder kommt das Thema KI bei selbstfahrenden Autos in die Schlagzeilen der Medien. Nicht nur sie, sondern auch die Autokonzerne beschäftigen sich mit den ethischen Folgen eines Unfalls. Wie würde das Auto dies entscheiden? Tötet es den Fahrer oder das Opfer vor dem Auto? Jedoch sollte laut einer [McKinsey-Studie](https://www.mckinsey.de/autonomes-fahren-veraendert-autoindustrie-und-staedte) die Unfallquote durch selbstfahrende Autos um 90 Prozent sinken. Das jedoch selbstfahrende Autos in allernächster Zeit Realität werden, da sind sich die Skeptiker der Technik sicher.

Ein starker Befürworter der Künstlichen Intelligenz Jeff Bezos, Gründer von Amazon, behauptet, dass es keine Institution auf dem Planeten, keine Behörde, kein Unternehmen gäbe, welches nicht durch Künstliche Intelligenz verbessert werden könne.

Eines der größten Themen, wenn es um Künstliche Intelligenz geht oder allgemein um Technische Evolution ist das Verschwinden von Arbeitsplätzen. Viele Arbeitsplätze, die es jetzt gibt, werden durch KIs nicht mehr benötigt werden. Durch die Digitalisierung entstehen knapp 58 Millionen neue Jobs, jedoch fallen dafür 78 Millionen Stellen weg. Durch den Einsatz von Künstlicher Intelligenz werden ganze Berufsgruppen abgelöst. Besonders betroffen sind Jobs die hoch kompliziert, routiniert oder körperlich anstrengend sind. Dagegen sind Berufe, die eine hohe soziale Kompetenz haben relativ sicher.

Wie Yann LeCun, KI-Forschungschef von Facebook zur Beruhigung sagte: „Künstliche Intelligenz wird die menschliche Intelligenz erweitern, nicht ersetzen – in der gleichen Weise, wie jedes neue Instrument unsere Fähigkeiten vergrößert.“

Die Zukunft ist technisch. Bereits in der Schule werden Kinder auf das technische Zeitalter vorbereitet. Programmieren lernen Kinder heute schon in Ansätzen in der Grundschule, Informatik ist einer der gefragtesten Studiengänge. Er ist verzweigter, es gibt Varianten für werdende Lehrer, Geschäftsleute, Mediziner.

Auch das Studieren ist weitaus üblicher als noch vor einer Generation. Es gibt Vorlesungen auf höchstem Niveau im Internet und das von internationalen Fachgrößen. Oft hören sich eine Vorlesung bis zu Zehntausend Menschen an und absolvieren Prüfungen. Die Sprache ist dabei kein Problem mehr, weil hochintelligente Programme simultan und fehlerfrei von jeder Sprache in jede erwünschte andere übersetzen.

Ein weiterer positiver Schritt ist die für viele Menschen erschwinglich gewordene medizinische Versorgung. Bereits 2017 flackerte das Potential gelegentlich auf, zum Beispiel in einem Test des Supercomputers Watson, den IBM gemeinsam mit dem New York Genome Center durchführte und der ein beachtliches Ergebnis erbrachte: Um das Erbgut eines 76 Jahre alten Patienten mit lebensgefährlichem Gehirntumor zu analysieren und eine Therapie vorzuschlagen, brauchte Watson bloß zehn Minuten – ein menschlicher Experte dagegen 160 Stunden Arbeitszeit. Ein riesiger Unterschied, der Leben retten kann. Heute ist das bereits Standard und geht noch schneller.

Ein weiteres brisantes Feld rund um das Thema intelligente Maschinen ist das Ungleichgewicht zu den Fähigkeiten des natürlichen Menschen. Wird Biotech soziale Ungerechtigkeit stiften?

Eine der viel diskutierten Zukunft Szenarien stammt vom israelischen Historiker und Bestsellerautor Yuval Noah Harari. Laut Harari haben Menschen nur eine Möglichkeit, sich vor übermächtigen intelligenten Maschinen zu schützen: Sie dürfen nicht mehr auf ihre natürlichen, angeborenen Fähigkeiten vertrauen, sondern Körper und Geist aufrüsten. Unter zu Hilfenahme von Biotechnologie, Cyborg-Technologie und der Erzeugung nicht-organischer Lebewesen geht in er Konkurrenz. Was ist jedoch, wenn sich eine Gruppe von Menschen einer Gesellschaft das Aufrüsten finanziell nicht leisten kann? Der gesellschaftliche Sprengstoff ist extrem und vorhersehbar.

Nach Hararis Annahme reagiert der menschliche Geist auf Errungenschaften nicht mit Zufriedenheit, sondern mit dem Verlangen nach Mehr. Können wir unser Streben nach immer größeren Biotech-Fortschritten zügeln? Harari meint: nein. Laut Experten werden wir im späten 21. Jahrhundert bereits durchschnittlich 150 Jahre alt, unsere Körper werden immer gesünder und jünger. Doch dieses „Upgrade“ bekommen nur diejenigen, die es sich leisten können. Viele Wissenschaftler befürchten, dass biologische Kasten entstehen, die Reiche und Gesunde von Armen und Kranken trennen.

Ende des 21. Jahrhunderts werden alle wirtschaftlich relevanten Berufe durch effektive Roboter abgedeckt. Der Mensch muss sich daran gewöhnen, dass er Dinge erschafft, die immer kompliziertere Aufgaben besser bewältigen als er selbst.

Nur die Philosophen bekamen eine spannende neue Frage hinzu: Ist das auf dem Rechner „lebende“ Programm nur Künstliche Intelligenz oder doch ein Mensch, da er Emotionen entwickeln kann?

#### Quellen

- https://www.zeit.de/2017/47/kuenstliche-intelligenz-kybernetiker-robert-trappl
- https://www.faz.net/aktuell/wirtschaft/me-convention/kuenstliche-intelligenz-die-zukunft-wird-schlauer-15191551.html
- https://www.heise.de/newsticker/meldung/KI-Konferenz-IJCAI-Die-moegliche-Zukunft-der-Kuenstlichen-Intelligenz-4115536.html
- https://www.gruenderszene.de/gs-connect/audible/alltag-mit-kuenstlicher-intelligenz-in-der-zukunft-2018-10767
