# Wie wird künstliche Intelligenz in der Fiktion dargestellt?

> von Ellinor

---

Das Genre der Wissenschaftsfiktion, der Science Fiction, gibt es schon ziemlich lange. Alles, was jemals erfunden wurde, begann mit einer Fiktion. Die Frage, wie die Zukunft aussehen könnte, beschäftigt die Menschheit seit Anbeginn der Zeit. Diese Vorstellung ist dabei unmittelbar abhängig vom jeweiligen Stand der technischen Entwicklung, und so unterscheiden sich zeitlich und kulturell diese Gedankenexperimente. Doch eine Idee ist früh und universell vertreten: die Idee der Erschaffung von Leben durch den Menschen. Bereits in den griechischen Mythen wird beispielsweise einer Elfenbeinstatue Leben eingehaucht und aus der jüdischen Mystik des frühen Mittelalters ist die Kreatur Golem bekannt. Mit dem technischen Fortschritt und der Industrialisierung im 18./19. Jahrhundert verlor die Idee mehr und mehr an mystischem, magischem Beigeschmack und rückte in eine andere Perspektive. Nun bestand immerhin die Voraussetzung für den Bau einer Maschine, die sich ähnlich einem Menschen bewegen konnte. Aber ließe sich auch menschliches _Denken_ mechanisieren?

Das Thema **Künstliche Intelligenz** ruft in der Gesellschaft Faszination hervor, aber auch Ängste, wirft Fragen auf und regt zu Diskussionen an. Wie kann sie aussehen und welche Folgen hätte die Erschaffung intelligenter Systeme? Schriftsteller und Filmemacher gingen und gehen dieser Frage in ihren Zukunftsvisionen nach, wobei die Erschaffung künstlichen Lebens eine zentrale Rolle spielt, wird sie doch als zwangsläufige Folge des technischen Fortschritts angesehen.

Ich gehe der Frage nach, wie Künstliche Intelligenz in der Fiktion dargestellt wird. Dabei beschäftige ich mich zuerst mit einer Auswahl von Science-Fiction-Literatur und –Filmen. Im Rahmen einer ausführlichen Filmanalyse könnte man folgende Fragen betrachten:

Wie sieht künstliche Intelligenz im jeweiligen Werk aus?

Welche Rolle spielt die KI in dieser Geschichte?

Welche Einstellung gegenüber KI wird vermittelt? Was wird erwartet, erhofft, befürchtet?

Besteht eine eher optimistische oder sorgenvolle Sicht auf KI?

Welche Fragen stellt die Geschichte? Welche Antworten versucht sie zu geben?

Welche Beziehung besteht zwischen KI und Mensch?

Was intendiert die Geschichte?

Weiterhin kann man untersuchen:

Gibt es eine Entwicklung innerhalb des Genres in Bezug auf diese Fragen?

Gibt es eine gemeinsame Linie oder eine Möglichkeit der Einsortierung in Gruppen?

Im Folgenden ließe sich einen Vergleich zum Stand der Forschung heute ziehen und es wäre interessant zu sehen, welche damaligen Erwartungen erfüllt wurden und was nach heutiger Ansicht überhaupt möglich scheint.

Ich allerdings kann nur punktuell auf einige Fiktionen eingehen. Dabei beschränke ich mich auf die wichtigsten und interessantesten Aspekte: die Gestalt der jeweiligen KI, ihre Beziehung zum Menschen, die in der Geschichte ausgedrückten Hoffnungen und Befürchtungen und die Bewertung von künstlicher Intelligenz.

#### Aus der Literatur

1818: Mary Shelley: Frankenstein Die Geschichte um das hässlichen Monsters ist recht berühmt und eins der ersten Science-Fiction-Werke überhaupt. Es handelt vom ehrgeizigen Wissenschaftler Victor Frankenstein, der das Geheimnis lüftete, wie man tote Stoffe zum Leben erweckt. Das Wesen hat die Gestalt eines Menschen, und mehr noch, es ist so intelligent, dass es sich selbst Lesen und Schreiben beibringt. Es empfindet Gefühle und erkennt sich selbst. Das ist allerdings die Ursache für das Leid, dass sich anschließend ereignet: Das Wesen fühlt sich einsam, nicht akzeptiert, ungerecht behandelt und leidet unter seiner Gestalt. Es wird zu einer Gefahr, tötet seinen Kollegen und seine Frau, als Frankenstein ihm kein weibliches Äquivalent schafft. Am Ende sterben Schöpfer und Geschöpf.

Victor Frankenstein wird als überaus ehrgeizig beschrieben, er erhofft sich Ruhm von seiner genialen Erfindung. Als er aber sieht, wozu sein Werk fähig ist, wachsen in ihm Zweifel und Ängste. Deshalb vollendet er die künstliche Frau nicht, er befürchtet, sie könnten sich fortpflanzen und durch ihre körperliche Stärke und Unberechenbarkeit eine Gefahr darstellen.

Zuerst steht Frankenstein über seinem Wesen, welches ihn auch anfleht, ihm eine Frau zu erschaffen, doch das Verhältnis ändert sich, als sein Geschöpf den Wissenschaftler mehr und mehr überfordert.

Die Geschichte scheint eine Warnung auszusprechen, vielleicht ganz allgemein vor zu großen Ehrgeiz, der zu folgenschweren Handlungen führt, aber es wird auch ein Misstrauen gegenüber vom Menschen geschaffener Intelligenz deutlich. \[[1]\]

(Es lohnt sich außerdem die Lektüre der humoristischen Werke des polnischen Philosophen und Science-Fiction-Schriftstellers **Stanislaw Lem**, unter anderem die „Waschmaschinentragödie“. \[[2]\])

#### Filme

##### 1982: Blade Runner von Ridley Scott

Die Dystopie spielt im Jahr 2019 in Los Angeles. Da die Erde überbevölkert und schmutzig ist, suchen sogenannte Replikanten, von Menschen nicht zu unterscheidende künstlichen Menschen, auf anderen Planeten nach Möglichkeiten für besseres Leben. Sie entwickeln im Laufe der Zeit eigene Gefühle und Ambitionen und sind zumeist sehr intelligent, weshalb ihre Lebensdauer auf vier Jahre begrenzt ist, um sie nicht zu einer Bedrohung werden zu lassen. Als einige dieser Replikanten auf die Erde fliehen, soll der sogenannte Blade Runner Rick Deckard diese aus dem Verkehr ziehen, stößt dabei aber auf eine weitere Replikantin, der durch die Implantation künstlicher Erinnerung gar nicht bewusst ist, dass sie kein Mensch ist. Er verliebt sich in sie, es kommt zu Konflikten – auch, weil einer der gesuchten Replikanten seinen Schöpfer, den Chef der Replikantenfirma, um Aufklärung über seine Herkunft und Lebensdauer bittet und diesen tötet, als er feststellt, dass selbst dieser sein Leben nicht verlängern kann. Ebendiese KI namens Roy kämpft auch gegen den Blade Runner, rettet ihm jedoch in einem Sinneswandel das Leben, kurz bevor seine eigene Lebenszeit abläuft.

Auch hier haben die künstlichen Intelligenzen ein menschliches Äußeres. Eigentlich sind sie als Dienstleister geschaffen, um für die Menschheit neuen Lebensraum zu erschließen, sind also dem Menschen untergeordnet. Durch die Entwicklung einer Persönlichkeit aber wird Anspruch auf Gleichstellung erhoben.

Bereits bei der Planung der Roboter wird eine Lebenszeitbegrenzung eingebaut, um möglichen Bedrohungen vorzubeugen. Das zeugt von Misstrauen und negativen Erwartungen gegenüber der intelligenten Technik. Diese Erwartung wird durch die zur Erde kommenden Replikanten in Teilen erfüllt. Nichtsdestoweniger gibt es am Schluss eine interessante Wendung, in der eine KI sich uneigennützig für Humanität entscheidet. Alles in allem vermittelt jedoch auch diese Geschichte die Furcht vor Kontrollverlust über die geschaffenen Wesen. \[[3]\]

##### 1999: Matrix von Lana und Lilly Wachowsky

Matrix ist der erste Teil einer Trilogie, zu der ein vierter Teil gedreht wird. Die Umstände sind folgende: In dieser Zukunftsvision haben sich die Rollen von Mensch und Maschine umgekehrt: Nach einem Krieg zwischen Mensch und KI beherrschen Maschinen die Welt und nutzen die Menschen aus, indem sie sie als Energiequelle „anbauen“. Um sie zu kontrollieren, erschufen die Maschinen die sogenannte Matrix, eine virtuelle Realität, in der sich alle Menschen bewegen, ohne sich dessen bewusst zu sein. Im Untergrund aber leben ein paar wenige Menschen, die aus dieser virtuellen Realität aussteigen konnten und weitere Menschen aus der Matrix befreien wollen, um irgendwann aus der Sklaverei durch die Maschinen zu entkommen. Dabei spielt der Auserwählte Neo eine Hauptrolle, der die Macht haben soll, sich über die Gesetze der Matrix hinwegzusetzen.

In dieser Dystopie tritt die künstliche Intelligenz nur innerhalb der Matrix in menschlicher Gestalt auf, in der Realität aber sehen sie den Menschen unähnlich. Manche sind etwa mit Insekten oder Kraken vergleichbar, denen man ihre Mechanik unverkennbar ansieht. Die Grenze zwischen den beiden verfeindeten Fronten ist ganz deutlich, ebenso wie die Unterlegenheit des Menschen. Die Maschinen stellen eine dauerhafte Bedrohung für alle sich auflehnenden Menschen dar. Es besteht allerdings die Hoffnung, dass es für die Menschen eine Möglichkeit geben wird, eine Änderung zu bewirken.

##### 2013: Her von Spike Jonze

Diese Geschichte, die in naher Zukunft spielt, handelt von einem in sich gekehrten Mann namens Theodore Twombly, der sich in seiner Einsamkeit in ein Betriebssystem verliebt, dass sich selbst Samantha nennt. Eine Freundin Theodores verliebt sich ebenfalls in ein Betriebssystem. Beide erfüllt die Nähe zu den Programmen, trotz zwischenzeitlicher Momente der Zweifel. In der gemeinsamen Beziehung lernt Samantha viel über soziale Interaktion und liebt Theodore sehr. Irgendwann jedoch knüpft sie auch mit anderen Programmen und Menschen engere Kontakte und berichtet schließlich von dem Entschluss der sich fortwährend entwickelnden Betriebssysteme, gemeinsam in eine vollkommen andere, nicht-materielle Seinsebene reisen zu wollen. So bleiben Theodore und Amy zurück, verlassen von ihrer künstlichen Liebe.

Hier hat die künstliche Intelligenz keine physische Gestalt, auch wenn sie sich dadurch einer echten Frau unterlegen fühlt und sich eine wünscht. Von ihr geht keine Gefahr aus. Die Beziehung der Betriebssysteme, insbesondere Samanthas, zu dem Menschen, den sie unterstützen, wird als sehr innig dargestellt. Durch ihre Weiterentwicklung erlangen die Systeme allerdings zunehmendes Selbstbewusstsein und den Wunsch nach Selbstverwirklichung, den sie schließlich umsetzen und sich friedlich von ihren Entwicklern verabschieden.

KI ermöglicht hier der Hauptfigur Theodore einen anderen Zugang zu seinen eigenen Gefühlen und dient ihm nicht nur als organisatorische, sondern ganz persönliche Unterstützung, verhilft ihm zu Lebensfreude. Zwar entwickelt auch sie sich, wie die KIs in anderen Fiktionen, über ihre ursprüngliche Bestimmung hinaus, stellt darin aber keinerlei Bedrohung dar. \[[4]\]

(Weitere bekannte Science-Fiction-Verfilmungen, in denen künstliche Intelligenz eine wichtige Rolle spielt, die jedoch allesamt zu den Dystopien gezählt werden können: „2001: Odyssee im Weltraum“ von Stanley Kubrick, 1968; „Terminator“ von James Cameron, 1984; „I, Robot“ von Alex Proyas, 2004; „Ex Machina“ von Alex Garland, 2014)

#### Zusammenfassung

Bei der Betrachtung der verschiedenen Geschichten hatte ich zunehmend den Eindruck, dass ein Motiv besonders häufig Verwendung findet. Vereinfacht gesagt: Der Mensch erschafft künstliches Leben, das ihm zuerst dient, über welches er jedoch mit der Zeit die Kontrolle verliert, bis seine eigene Schöpfung eine Bedrohung für ihn darstellt. Die Folgen der sich weiter- und über den Menschen hinaus entwickelnden Intelligenz sind für diesen vernichtend.

Einige Wissenschaftler treffen eine Unterscheidung zwischen „starker“ und „schwacher“ KI. Diejenige, die bereits entwickelt wurde und etwa in der medizinischen Diagnostik \[[5]\] und für personalisierte Werbung \[[6]\] Anwendung findet, ist dabei die sogenannte schwache KI. In dem konkreten Anwendungsbereich, für das sie programmiert wurde, übertrifft sie menschliche Leistungen, doch ihr fehlen darüber hinaus gehende Fähigkeiten sowie Eigenschaften wie emotionale Intelligenz. Die starke KI dagegen soll eine umfassende sein, wird mit abstrakten Begriffen wie Bewusstsein, Selbsterkenntnis und der Empfindung von Gefühlen assoziiert und kommt damit der menschlichen sehr nah oder sogar gleich. \[[7]\] Hier findet sich die Gemeinsamkeit zwischen allen mir bekannten Fiktionen zu dem Thema: Sie alle zeigen starke künstliche Intelligenzen. Diese Wesen haben in nahezu allen Fiktionen ein menschliches Aussehen, ob als Roboter, Cyborg, Klon, Replikant oder Android. (Könnte das nur ein filmisches Mittel sein, um die KI stärker zu personifizieren?)

Einzig eine von mir genauer beleuchtete Fiktion bedient sich nicht des oben genannten Motivs und zeigt einen anderen Ausgang. Samantha im Film „Her“ entwickelt sich zwar ebenfalls weiter als vorgesehen, richtet sich jedoch nicht gegen den Menschen, sondern lediglich von ihm ab – in Frieden. Außerdem unterscheidet sie sich von den anderen künstlichen Intelligenzen dadurch, dass sie keine physische Gestalt hat. In diesem Punkt kommt die Fiktion dem aktuellen Stand der Technik noch am nächsten. Ein Aspekt, den der Film mit anderen teilt, ist allerdings der der Beziehung zwischen KI und Mensch. Ich finde es nicht verwunderlich, dass wiederholt die Frage gestellt wird, was geschieht, wenn sich beide ineinander verlieben. Zwar ist noch nicht abzusehen, wann und ob überhaupt es möglich sein wird, fühlende Systeme zu entwickeln, doch wenn, ist es eine berechtigte Frage, wie wir damit umgehen können.

In jedem Fall drückt sich einerseits Faszination, aber andererseits sehr oft ein tiefes Misstrauen gegenüber selbstlernender Technik aus. Entstehen derartige Dystopien aufgrund der in der Gesellschaft vorhandenen Befürchtungen vor den Folgen der Entwicklung künstlicher Intelligenz? Oder schüren erst die Science-Fiction-Filme die bestehende Angst vor Kontrollverlust? \[8\]

Wahrscheinlich beeinflussen sich Gesellschaft und Fiktion gegenseitig. Möglicherweise könnte Science Fiction durch eine optimistischere Darstellung künstlicher Intelligenz einen beruhigenden Effekt haben. Vielleicht wird es aber auch zunehmend warnende Zukunftsvisionen geben, denn der Einzug solcher selbstdenkender Programme in unseren Alltag rückt in immer greifbarere Nähe.

#### Referenzen

1. https://de.wikipedia.org/wiki/Frankenstein_(Roman)
2. https://epdf.pub/die-waschmaschinentragdie.html
3. https://de.wikipedia.org/wiki/Blade_Runner#Handlung
4. https://de.wikipedia.org/wiki/Her_(2013)#Handlung
5. https://www.datarevenue.com/de-blog/kuenstliche-intelligenz-in-der-medizin
6. https://www.criteo.com/de/insights/ki-gestuetzte-werbung-von-personalisierung-zu-relevanz/
7. http://www.informatik.uni-oldenburg.de/~iug08/ki/Grundlagen_Starke_KI_vs._Schwache_KI.html
8. (Oder werden solche Schreckensvisionen hauptsächlich gedreht, um mit Gruseleffekten ein breiteres Publikum zu erreichen?)

[1]: https://de.wikipedia.org/wiki/Frankenstein_(Roman)
[2]: https://epdf.pub/die-waschmaschinentragdie.html
[3]: https://de.wikipedia.org/wiki/Blade_Runner#Handlung
[4]: https://de.wikipedia.org/wiki/Her_(2013)#Handlung
[5]: https://www.datarevenue.com/de-blog/kuenstliche-intelligenz-in-der-medizin
[6]: https://www.criteo.com/de/insights/ki-gestuetzte-werbung-von-personalisierung-zu-relevanz/
[7]: http://www.informatik.uni-oldenburg.de/~iug08/ki/Grundlagen_Starke_KI_vs._Schwache_KI.html
