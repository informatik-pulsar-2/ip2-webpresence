# Wie motivieren wir uns, und was braucht es damit KI selber Motive entwickeln kann?

> von Maria & Alena

---

Motivation ist ein treibender Grundstoff unseres Lebens. Sie bringt uns morgens aus dem Bett und Nachmittags auf den Fußballplatz oder ins Kino. Aber was ist diese Kraft, die uns antreibt eigentlich? Und vor allem: Woher kommt sie?

Als Motivation fasst man alle kognitiven, sozialen und emotionalen Faktoren zusammen, die Verhalten veranlassen oder direkt beeinflussen. Unabhängig von der Art des Verhaltens unterscheidet man zwischen biologischen und erlernten Motiven.

Aus biologischer Sicht macht man alles um Schutz zu finden, den Hunger und den Durst zu stillen, sowie aus Sexualität und Neugierde. Gelernte Verhaltensweisen können ebenso starke Triebe hervorrufen etwas zu tun, wie biologische Bedürfnisse. Zu den gelernten Verhaltensweisen gehören z.B. positive Erfahrungen. Diese werden im emotionalen Gedächtnis abgespeichert, im sogenannten präfrontalen Cortex. Je mehr von diesen sogenannten Erfolgserinnerungen abgespeichert sind, desto besser kann man sich motivieren. „Je besser man etwas kann, je mehr Spaß es macht und je mehr Sinn für einen Menschen darin liegt, desto höher ist sein Antrieb“, sagt Hugo Kehr, Psychologe an der TU München.

Auch verschiedene Emotionen können die Motivation beeinflussen. So kann Angst unterbewusst oder in geringer Form motivierend wirken, jedoch, wenn sie zu häufig oder zu stark auftritt, demotivierend sein und einen buchstäblich lähmen.

Bis vor kurzem dachte man, dass Motivation ausschließlich vom Belohnungssystems ausgeht. (Das Belohnungssystem ) Dieses ist ein verzweigtes Netz aus Hirnarealen und Neuronen. Ein wichtiger Teil des Belohnungssystems ist der sogenannte Nucleus accumbens. Er steuert Emotionen wie Verlangen, Gier und Vorfreude. Wenn die Rezeptoren des Nucleus accumbens von Botenstoffen wie z.B. Dopamin stimuliert werden sendet das System Erregungspotentiale an die zuständigen Bereiche des Gehirns.

Der zweite wichtige Teil des Belohnungssystems ist das ventrale Striatum. Hier entsteht der Selbstantrieb eines Menschen. Forscher haben hier drei zentrale Selbstmotivatoren ausgemacht: Anschluss, Anerkennung und Macht.

Die Theorie, dass das Belohnungssystem der einzige Ursprung unserer Motivation ist, ist jedoch veraltet. Verschiedene Studien und Gehirnscans haben ergeben, dass unsere Motivation beziehungsweise unser Wille und Antrieb nicht nur durch das Belohnungssystem entsteht. Es gibt vielmehr eine Art Schaltkreis der Motivation in unserem Kopf, in den viele Teile des Gehirns involviert sind. Das Belohnungssystem ist ein Teil davon. Mitwirken tut aber auch das Limbische System, welches aus verknüpften Strukturen in Großhirn, Mittelhirn und Hirnstamm besteht. Die wichtigsten Bestandteile des limbischen Systems sind der Hippocampus, der Hypothalamus und die Amygdala. Im Limbischen System werden u.a. Schmerzinformationen, unbewusste und emotionale Inhalte miteinander verknüpft. Einen weiteren wichtigen Bestandteil dieses Motivationsschaltkreises bilden die Basalganglien. Sie sind eine Gruppe von Kernen im Großhirn, die unter der Großhirnrinde liegen. Die Basalganglien werden hauptsächlich mit der Willkürmotorik, also den bewusst gesteuerten Bewegungen, in Verbindung gebracht.

Wie aktiv beziehungsweise inaktiv die verschiedenen Teile dieses Motivationsschaltkreises sind, wie sehr diese gereizt werden oder verkümmern und wie schnell und intensiv die Botenstoffe zwischen den Synapsen ausgeschüttet werden, hat alles einen erheblichen Einfluss auf das Verhalten einer Person.

Kurz gesagt ist Motivation aus biologischer Sicht also der Wunsch, Bedürfnisse zu erfüllen.

_Aber wie ist das bei Künstlichen Intelligenzen?_

Sieht man sich Sciencefiction-Filme an, gibt es in diesen häufig das Motiv der von Menschen erschaffenen Künstlichen Intelligenz, die selbstständig agiert und ihre eigenen Ziele und Motive hat. Aber ist eine solche Maschine wirklich möglich? Kann KI selbst Motive entwickeln und somit Motivation haben?

Diese Frage ist aus heutiger Sicht nicht eindeutig zu beantworten. Man kann sie lediglich anhand von Fakten auf einer philosophischen Basis diskutieren. Um das tun zu können, ist es hilfreich zu wissen, wie heutzutage die meisten Künstlichen Intelligenzen (KI) grundsätzlich funktionieren.

Das System ist, wie so oft simpel: Ein Mensch schreibt einen Code für eine Maschine, welche die im Code einprogrammierten Befehle ausführt. Durch eine gewisse Menge an, im sogenannten „Training“, in das Programm der Maschine eingeführten Daten, lernt die KI, ihre Fähigkeiten nach und nach immer weiter auszubauen und zu perfektionieren, bis sie das ihr vorgesetzte Problem oder die ihr zugedachte Aufgabe erfüllen kann. Dabei unterscheidet man zwischen starker und schwacher Künstlicher Intelligenz. Letztere versucht lediglich Probleme zu lösen ohne das Ziel eines menschliches Bewusstsein oder ähnliches zu erlangen. In den letzten Jahren wurden auf diesem Gebiet große Fortschritte gemacht.

Wenn wir über motivierte KI sprechen, reden wir jedoch von sogenannten starken Künstlichen Intelligenzen, welche mit dem Ziel entstehen ein menschenähnliches Bewusstsein und eine mindestens menschenähnliche Intelligenz zu besitzen, wobei das Ausmaß der Intelligenz nach oben hin offen ist.

Sieht man sich Lebewesen an, so gibt es bei ihnen für Motivation bestimmte Auslöser:

- streben nach bestimmten „guten“ Zuständen
  - Stillen von Grundbedürfnissen
  - Erreichen von gesetzten Zielen
- Überwinden von Schwierigkeiten

Für die Funktion dieser Auslöser werden bestimmte Voraussetzungen benötigt:

- Bedürfnisse
- Ziele / Wünsche und die Fähigkeit diese zu entwickeln
- Bewusstsein für sich selbst und Umgebung
  - Wunsch nach Befriedigung von Bedürfnissen, Verbesserung o.ä.
- Lösungsorientiertes Denken
- Eigener Wille und eigene Vorstellungen, Überzeugungen und Maßstäbe
- Wahrnehmung von Reizen der Außenwelt
- Wunsch nach Belohnung (z.B. in Form von erfüllten Zielen)
  - Belohnungssystem
- Verlangen nach positiven Emotionen
- Empfinden von Begierde, Interesse oder Neugierde, sowie Emotionen und Empathie
- Gedächtnis mit Erinnerungsvermögen

Ist ein Wesen zu diesen Voraussetzungen nicht fähig oder sind diese in zu geringem Maß erfüllt, so kann es, durchaus als nicht motivations- und somit auch als nicht lebensfähig eingestuft werden.

Nun ist KI aber kein Lebewesen, sondern eine Maschine die, nach heutigem Stand der Technik, keinerlei Grundbedürfnisse im eigentlichen Sinne hat, abgesehen von einer konstanten Versorgung mit Energie in Form von Strom. Ihr fehlt damit eine der wichtigsten Voraussetzungen für Motivation, denn ohne die Existenz von Bedürfnissen, können diese auch nicht erfüllt werden.

Auch haben KI bisher keine eigenen Emotionen, sondern können diese lediglich durch Algorithmen imitieren. Sie denken also auf aktuellem Stand nicht selbst und haben auch keinerlei eigene Empathie oder Moral, denn all dies setzt, ebenso wie Emotionen, einen eigenen Willen und ein Bewusstsein mit eigenen Vorstellungen, Überzeugungen und Maßstäben voraus. Bei KI ist das alles nur in geringem Maße vorhanden oder fehlt ganz.

Man kann künstlicher Intelligenz zwar durch das Einspeisen bestimmter Datensätze z.B. moralische Grundsätze beibringen, jedoch übernimmt die Maschine dabei lediglich die Vorstellungen und Überzeugungen des Datengebers und entwickelt kein eigenes Verständnis davon.

Dafür ist ein Bewusstsein auf Datenbasis bei KI vorhanden, sie wissen beispielsweise ihren eigenen Akkustand. Auch gibt es künstliche Intelligenzen, die in einer idealisierten, digitalen Welt mit Hilfe von neuronalen Netzwerken in der Lage sind Bedürfnisse, Emotionen und Motivation zu haben, jedoch ist dies in der realen Welt bisher nicht umsetzbar, da die obengenannten dafür benötigten Fähigkeiten nahezu vollständig fehlen. Um Dinge wie eigenes Bewusstsein oder eigenen Willen, aber auch menschliche Intelligenz an eine Maschine weitergeben zu können, müsste es eine klare Definition dieser Begriffe geben und die Bereiche müssten allesamt bis ins kleinste Detail erforscht sein, sodass die erkannten Strukturen auf eine Maschine übertragen werden könnten. Allerdings befinden sich diese Bereiche noch stark im Diskurs und sind teils kaum erforscht.

Denn was ist tatsächlich freier Wille? Ist es die Fähigkeit selbst Entscheidungen zu fällen?

Die Fähigkeit, sich gegen Dinge zu stellen? Oder ist es doch etwas ganz anderes?

Solange solche Fragen noch im Raum stehen, ist es nicht wirklich möglich so etwas zu programmieren. Durch das Fehlen der Emotionen fällt auch das Streben nach positiven Sinneswahrnehmungen vollkommen weg, ebenso wie Wünsche, da diese meist aus Emotionen heraus entstehen.

Eine weitere Voraussetzung für Motivation ist die Wahrnehmung der Außenwelt und die Reaktion auf diese. Roboter können ihre Umgebung mittels Sensoren und Kameras gut wahrnehmen und sind in der Lage mehrere Wahrnehmungsbereiche, wie z.B. Motorik und Sensorik, zu verknüpfen, um sich gefahrlos fortbewegen zu können. Allerdings sind ihre motorischen Fähigkeiten längst noch nicht so ausgereift, dass sie an menschliche heranreichen und auch in der sozialen Interaktion gibt es noch viele Baustellen. Sozial interagierende KI existieren zwar bereits, sie sind jedoch noch nicht besonders weit fortgeschritten. So gibt es beispielsweise einen Roboter namens Sophia, der zwar Interviews geben kann, aber mit Dingen wie unterschwelliger Ironie oder rhetorischen Fragen wahrscheinlich überfordert wäre. Ein großes Problem ist auch, dass KI sich noch nicht nicht in andere hineinversetzen können, was ein wichtiger Teil menschlicher Interaktion ist.

KI haben ebenso wie Lebewesen eine Art Gedächtnis in Form von Datenspeicherung. Die Intelligenzen können anhand ihrer Erfahrungen dazu lernen. Es gibt auch Dinge, in denen KI jetzt schon Menschen übertreffen. Beim lösungsorientierten Denken agieren sie wesentlich schneller und auch was das Erlernen neuer Dinge angeht sind sie erstaunlich schnell. Allerdings ist dieses Lernen momentan auf das Lösen einer bestimmten Aufgabe fixiert und diesbezüglich noch nicht wirklich flexibel. Dieser Punkt hält KI im Moment noch davon ab sich selber Ziele zu setzten.
KI haben keine direkten Bedürfnisse, wie schon oben festgestellt, jedoch gibt es etwas Vergleichbares bei ihnen, das gleichzeitig mit dem Bestreben Ziele zu erreichen oder Wünsche zu erfüllen vergleichbar ist: den Erfolg bzw. der Prozess der Lösungssuche.
Ohne diesen Prozess gibt es für die Maschine keinen Grund etwas zu tun und somit auch keine Motivation. Für diese Lösungssuche brauchen sie jedoch Ziele. Sind diese gegeben, kann sich eine KI selbstständig weiterentwickeln, jedoch nur solange bis das Ziel erreicht ist. Dieser Punkt ist mit dem Wunsch nach Belohnung vergleichbar.

Um sich selbst Ziele zu setzen und sich eigenständig weiterzuentwickeln benötigt die KI jedoch einen eigenen Willen und ein Bewusstsein, die, wie bereits erwähnt, fehlen.
Aus diesem Grund wird im Moment viel in Richtung eigene Zielsetzung und selbstständige Produktion in der IT-Branche gearbeitet, denn das Ziel ist immer noch das einer starken Intelligenz, also die technische Nachstellung des Menschen.

Menschenähnlichere Roboter die sozial agieren und Intelligent sind, sozusagen eine Art Weiterentwicklung von Sophia, wird es in naher Zukunft sehr wahrscheinlich geben, sowie die weitere Verbesserung von vorhandenen Fähigkeiten, wie Motorik oder Mimik.
In weiterer Zukunft sind auch sozial interagierende KI, die sich eigenständig weiterentwickeln und „denken“ und die sogar die Intelligenz ihrer Erschaffer übertreffen denkbar.
Die einzige Voraussetzung: eine KI mit annähernd menschlicher Intelligenz. Das ist zwar aktuell noch nicht möglich, es wird aber in diese Richtung gearbeitet und geforscht und wenn die Entwicklung weitergeht wie bisher angenommen, sind auch Roboter mit emotionsähnlichem Verhalten denkbar. Nahezu undenkbar sind hingegen KI mit eigenen Emotionen, die nicht simuliert sind und Identisch zum Menschen aufgebaute KI, die die selbe Evolution durchlaufen und den selben Aufbau haben wie wir.

Fasst man alles zusammen, so sieht man, dass eine KI mit eigenen Motiven wahrscheinlich nicht in naher Zukunft möglich sein wird. Zu vieles ist noch zu wenig ausgereift, zu viele wichtige Dinge wie etwa Bewusstsein, Emotionen, Empathie oder freier Wille sind noch nicht genau genug definiert. Nach aktuellen Schätzungen werden Menschen in 50-100 Jahren fähig sein, KI mit „Intelligenzlevel Mensch“ zu entwickeln, und folglich auch mit eigener Zielsetzung, Motivation und möglicherweise sogar Reproduktion. Ob das uns nun alles erleichtern wird oder ob es unser Untergang sein wird lässt sich momentan noch nicht sagen, aber man sollte über den vielen Vorteilen von KI auf keinen Fall ihre Nachteile vergessen.

#### Quellen

##### dasgehirn.info

- https://www.dasgehirn.info/denken/motivation/triebfedern-des-tuns
- https://www.dasgehirn.info/denken/motivation/schaltkreise-der-motivation
- https://www.dasgehirn.info/denken/motivation
- https://3d.dasgehirn.info/#brainPath=anatomie

##### YouTube

- https://www.youtube.com/watch?v=YxyGwH7Ku5Y
- https://www.youtube.com/watch?v=ZoemTySxFso

##### Planet wissen

- https://www.planet-wissen.de/technik/computer_und_roboter/kuenstliche_intelligenz/pwiekuenstlicheintelligenzundbewusstsein100.html

##### Ted talks

- https://www.nytimes.com/2016/12/14/magazine/the-great-ai-awakening.html?_r=0
- https://www.youtube.com/watch?v=4QjZDUaDxQU
- https://www.ted.com/playlists/613/what_are_emotions
- https://www.ted.com/talks/ray_kurzweil_get_ready_for_hybrid_thinking?referrer=playlist-talks_on_artificial_intelligen
- https://www.ted.com/talks/sam_harris_can_we_build_ai_without_losing_control_over_it?referrer=playlist-talks_on_artificial_intelligen
- https://www.youtube.com/watch?v=GUApSRMWxqo
- https://www.youtube.com/watch?v=BfDQNrVphLQ
- https://www.lead-digital.de/koennen-ki-fuehlen/

##### Sonstige

- https://salvere.swiss/wie-funktioniert-motivation/
- https://www.androidpit.de/kuenstliche-intelligenz-geschichte-definitionen-einsatzgebiete
- https://www.nytimes.com/2016/12/14/magazine/the-great-ai-awakening.html?_r=0<
- https://www.ibusiness.de/aktuell/db/190842grollmann.html
- https://de.wikipedia.org/wiki/Künstliche_Intelligenz
- https://www.deutschlandfunk.de/kuenstliche-intelligenz-moral-ohne-bewusstsein.684.de
- https://www.weltwunder.de/photo_stories/wo-entsteht-mein-antrieb-und-was-bremst-ihn-aus
- http://gehirn-und-denken.de/motivation/
