from random import random as rnd

SAMPLES_PER_NUMBER = 10

data_path = "mnist_test_wRotations.csv"

data_file = open(data_path, 'r')
data_list = data_file.readlines()
data_list_len = len(data_list) - 1
data_file.close()

arrs = [[] for i in range(10)]

def arrFilled(i):
    if len(arrs[i]) < SAMPLES_PER_NUMBER:
        return False

    return True

def arrsFilled():
    for i in range(10):
        if not arrFilled(i):
            return False
    
    return True

for recordI in range(0, data_list_len, 3):
    randI = int(rnd() * 10) % 3

    record = data_list[recordI + randI]

    all_values = record.strip().split(',')

    label = all_values[0]
    labelI = int(label)
    img = all_values[1:]

    if not arrFilled(labelI):
        arrs[labelI].append(img)
    
        if arrFilled(labelI):
            print("Filled arr for", labelI)
    
    if arrsFilled():
        break

print(len(arrs))
print(",".join(map(str, [len(i) for i in arrs])))

jsarr_file = open("mnist_test_arr.js", "w")

jsarr_file.write("module.exports = [")
for i in arrs:
    jsarr_file.write("\n[")
    for r in i:
        jsarr_file.write("[" + ",".join(map(str, r)) + "],\n")
    jsarr_file.write("],\n")
jsarr_file.write("]")

jsarr_file.close()
