import numpy as np
import sys

from multilayer_network import multilayerNetwork as NN

np.random.seed(0)

n = NN([1, 1, 1], 1.0)

n.load()



nn_input = sys.argv[1]

def str_list_to_arr(s):
    s_arr = np.array(s.split(","), dtype="uint8")
    img_arr = s_arr.reshape((28, 28))
    
    return img_arr

img_in = str_list_to_arr(nn_input)

img_array = img_in.flatten()

img_array2 = ((img_array) / 255.0 * 0.99) + 0.01

testimg = img_array2

testoutputs = n.query(testimg)
testlabel = np.argmax(testoutputs)

print("\n".join([str(i) + ": " + str("{0:.2f}".format(round(e[0] * 100, 2))) + "%" for (i, e) in enumerate(testoutputs)]))
print("\n>>", testlabel)
