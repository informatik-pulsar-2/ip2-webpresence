# -*- coding: utf-8 -*-

import socket
import threading
from sys import exit

class ThreadedClient(object):
    def __init__(self, host="localhost", port=8123, size=1024):
        self.host = host
        self.port = port
        self.size = size

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def input(self, msg):
        print(msg)
        return input()

    def Wahl(self):
        Item = 0
        while not(Item in [1, 2, 3]):
            try:
                Item = self.input("Schere = 1, Stein = 2, Papier = 3\n")
                try:
                    Item = int(Item)
                except:
                    Item = 0
                if Item == 1:
                    print("Schere\n")
                elif Item == 2:
                    print("Stein\n")
                elif Item == 3:
                    print("Papier\n")
                else:
                    print("1, 2 ODER 3")
            except KeyboardInterrupt:
                break

        return Item

    def Game_loop(self):
        print(self.receivefun())

        while True:
            lobbyId = self.input("Lobby ID: ")
            self.send(lobbyId)
            lobbyJoined = self.receivefun()
            if lobbyJoined == "1":
                print(lobbyId)
                break
            else:
                print("Lobby voll!")

        while True:
            A = self.Wahl()
            self.send(A)

            winner = self.receivefun()
            if winner == None:
                print("opponent or server disconnected!")
                break
            print(winner)

            continueloop = self.receivefun()
            if continueloop == "1":
                print("Du hast dieses Match gewonnen!")
                break
            elif continueloop == "2":
                print("Du hast dieses Match verloren!")
                break
        
        print("exit.")
        exit()

    def receivefun(self):
        try:
            data = self.sock.recv(self.size)
            if data:
                msg = data.decode("utf-8")
                return msg
            else:
                print("disconnected")
                raise Exception("server disconnected")
        except:
            self.sock.close()
            return

    def run(self):
        self.sock.connect((self.host, self.port))
        threading.Thread(target = self.Game_loop).start()

    def send(self, msg):
        smsg = str(msg).encode("utf-8")
        self.sock.send(smsg)

if __name__ == "__main__":
    # host = input("host: ")
    # port = 8123
    # while True:
    #     port = input("port: ")
    #     try:
    #         port = int(port)
    #         break
    #     except ValueError:
    #         pass

    ThreadedClient().run()
