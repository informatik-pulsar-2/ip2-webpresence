import socket
import threading

###
# FROM https://stackoverflow.com/a/23828265/5712160
###

class ThreadedServer(object):
    def __init__(self, host="0.0.0.0", port=8123, size=1024):
        self.host = host
        self.port = port
        self.size = size

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

        self.lobbies = {}
    
    def joinLobby(self, lobbyId, client):
        # create a lobby by id, if it doesn't yet exist
        if not lobbyId in self.lobbies.keys():
            self.lobbies[lobbyId] = {
                "clients": [],
                "eingaben": [0, 0],
                "scores": [0, 0]
            }
        
        if len(self.lobbies[lobbyId]["clients"]) < 2:
            # join lobby by id, if spot left
            self.lobbies[lobbyId]["clients"].append(client)

            return self.lobbies[lobbyId]["clients"].index(client) + 1
        else:
            return False

    def closeLobby(self, lobbyId):
        if lobbyId:
            if lobbyId in self.lobbies.keys():
                for cclient in self.lobbies[lobbyId]["clients"]:
                    cclient.close()

                return self.lobbies.pop(lobbyId)
        
        return False

    def listen(self):
        self.sock.listen(10)
        print("Server started!", self.host, self.port, self.size)

        while True:
            client, address = self.sock.accept()

            threading.Thread(target = self.listenToClient, args = (client, address)).start()

    def Spiel(self, W, O):
        if W == O:
            print("Unentschieden")
        elif W == 1 and O == 2:
            print("Schere vs Stein")
            return 2
        elif W == 1 and O == 3:
            print("Schere vs Papier")
            return 1
        elif W == 2 and O == 1:
            print("Stein vs Schere")
            return 1
        elif W == 2 and O == 3:
            print("Stein vs Papier")
            return 2
        elif W == 3 and O == 1:
            print("Papier vs Schere")
            return 2
        elif W == 3 and O == 2:
            print("Papier vs Stein")
            return 1

    def listenToClient(self, client, address):
        # print("Connection from " + str(address))
        self.send(client, "Willkommen bei 'Schere, Stein, Papier'!")

        lobbyId = None
        indexInLobby = -1
        oIndexInLobby = -1

        while True:
            try:
                data = client.recv(self.size)

                if data:
                    msg = data.decode("utf-8")
                    print(str(address) + ": " + msg)
                    
                    if lobbyId == None:
                        indexInLobby = self.joinLobby(msg, client)
                        if indexInLobby != False:
                            lobbyId = msg
                            indexInLobby -= 1
                            oIndexInLobby = (indexInLobby + 1) % 2
                            self.send(client, "1")
                        else:
                            self.send(client, "0")

                        continue

                    try:
                        msg = int(msg)
                    except:
                        continue
                    if msg not in [1, 2, 3]:
                        continue

                    if self.lobbies[lobbyId]["eingaben"][indexInLobby] == 0:
                        self.lobbies[lobbyId]["eingaben"][indexInLobby] = msg

                        if self.lobbies[lobbyId]["eingaben"][oIndexInLobby] != 0:
                            W = self.lobbies[lobbyId]["eingaben"][indexInLobby]
                            O = self.lobbies[lobbyId]["eingaben"][oIndexInLobby]

                            # run the round
                            winner = self.Spiel(W, O)

                            if len(self.lobbies[lobbyId]["clients"]) == 2:
                                oClient = self.lobbies[lobbyId]["clients"][oIndexInLobby]

                                if winner == 1:
                                    self.lobbies[lobbyId]["scores"][indexInLobby] += 1
                                    self.send(client, "Du gewinnst!")
                                    self.send(oClient, "Du verlierst!")
                                elif winner == 2:
                                    self.lobbies[lobbyId]["scores"][oIndexInLobby] += 1
                                    self.send(client, "Du verlierst!")
                                    self.send(oClient, "Du gewinnst!")
                                else:
                                    self.send(self.lobbies[lobbyId]["clients"], "Unentschieden!")
                                
                                if self.lobbies[lobbyId]["scores"][indexInLobby] >= 3:
                                    self.send(client, 1)
                                    self.send(oClient, 2)

                                    self.closeLobby(lobbyId)
                                elif self.lobbies[lobbyId]["scores"][oIndexInLobby] >= 3:
                                    self.send(client, 2)
                                    self.send(oClient, 1)

                                    self.closeLobby(lobbyId)
                                else:
                                    self.send(self.lobbies[lobbyId]["clients"], 0)
                            else:
                                # someone left
                                pass
                            
                            self.lobbies[lobbyId]["eingaben"][indexInLobby] = 0
                            self.lobbies[lobbyId]["eingaben"][oIndexInLobby] = 0
                else:
                    print(str(address) + " disconnected")
                    raise Exception("client disconnected")
            except Exception as e:
                print("Closing connection to " + str(address), e)
                
                self.closeLobby(lobbyId)

                break
    
    def send(self, clients, msg):
        smsg = str(msg).encode("utf-8")
        if type(clients) == list:
            for cclient in clients:
                cclient.send(smsg)
        else:
            clients.send(smsg)

if __name__ == "__main__":
    ThreadedServer().listen()
