const express = require("express");
const app = express();
const compression = require("compression");
const fs = require("fs");
const { PythonShell } = require("python-shell");
const UUID = require("uuid").v4;
const marked = require("marked");

marked.setOptions({
    gfm: true,
    smartLists: true,
    breaks: true
});

app.set("view engine", "ejs");

app.use(compression());
app.use("/assets", express.static("assets"));
app.use("/", express.static("static"));

const articles = {
    "heilung-oder-untergang": {
        path: "heilung-oder-untergang",
        contentPath: __dirname + "/projects/heilung-oder-untergang.md",
        readingMode: true,
        title: "Künstliche Intelligenz - Heilung der Welt oder ihr Untergang?",
        authors: ["Luna"],
        shortContent:
            "KI. Wo die einen begeistert sind und unerschöpfliche Möglichkeiten sehen, fürchten die anderen, dass das Wissen einer Maschine unsere Vorstellung von Intelligenz übertreffen könnte.",
        content: ""
    },
    "audio-erkennung": {
        path: "audio-erkennung",
        contentPath: __dirname + "/projects/audio-erkennung.md",
        readingMode: true,
        title: "Audio Erkennung",
        authors: ["Sarah", "Selin"],
        shortContent:
            "Wie verstehen Sprachassistenten eigentlich, was ich gesagt oder gefragt habe, und wie können sie Musik im Bruchteil einer Sekunde betiteln?",
        content: ""
    },
    umfrage: {
        path: "umfrage",
        contentPath: __dirname + "/projects/umfrage.md",
        readingMode: true,
        title: "Eine Umfrage",
        authors: ["Ben", "Francis"],
        shortContent:
            "„Wie ist die allgemeine Stimmung der Berliner zum Thema Künstliche Intelligenz?“. Eine Auseinandersetzung durch 5 Fragen.",
        content: ""
    },
    "ki-in-fiktion": {
        path: "ki-in-fiktion",
        contentPath: __dirname + "/projects/ki-in-fiktion.md",
        readingMode: true,
        title: "Wie wird Künstliche Intelligenz in der Fiktion dargestellt?",
        authors: ["Ellinor"],
        shortContent:
            "Alles, was jemals erfunden wurde, begann mit einer Fiktion. Die Frage, wie die Zukunft aussehen könnte, beschäftigt die Menschheit seit Anbeginn der Zeit. Diese Vorstellung ist dabei unmittelbar abhängig vom jeweiligen Stand der technischen Entwicklung, und so unterscheiden sich zeitlich und kulturell diese Gedankenexperimente. Doch eine Idee ist früh und universell vertreten: die Idee der Erschaffung von Leben durch den Menschen.",
        content: ""
    },
    "motivation-und-motive": {
        path: "motivation-und-motive",
        contentPath: __dirname + "/projects/motivation-und-motive.md",
        readingMode: true,
        title:
            "Wie motivieren wir uns, und was braucht es damit KI selber Motive entwickeln kann?",
        authors: ["Maria", "Alena"],
        shortContent:
            "Motivation ist ein treibender Grundstoff unseres Lebens. Sie bringt uns morgens aus dem Bett und Nachmittags auf den Fußballplatz oder ins Kino. Aber was ist diese Kraft, die uns antreibt eigentlich? Und vor allem: Woher kommt sie?",
        content: ""
    }
};

const specialProjects = {
    pictures: {
        render: __dirname + "/public/pictures.ejs",
        hiddenProject: true,
        path: "/pictures/:frameId?",
        title: "Bilderreihe",
        authors: ["A", "B"],
        shortContent: "",
        pictureTree: {
            "1": {
                title: "",
                text: "Text for frame 1",
                img: "/assets/meme.png",
                imgAlt: "climate change doesn't matter ... if you stay indoors",
                next: "1.2"
            },
            "1.2": {
                title: "",
                text: "Text for frame 1.2",
                img: "/assets/meme.png",
                imgAlt: "climate change doesn't matter ... if you stay indoors",
                next: ["1.2.3-1", "1.2.3-2"]
            },
            "1.2.3-1": {
                title: "",
                text: "Text for frame 1.2.3-1",
                img: "/assets/meme.png",
                imgAlt: "climate change doesn't matter ... if you stay indoors",
                next: "1.2.3-1.2"
            },
            "1.2.3-1.2": {
                title: "",
                text: ":?",
                img: "/assets/meme.png",
                imgAlt: "climate change doesn't matter ... if you stay indoors",
                next: "0"
            },
            "1.2.3-2": {
                title: "",
                text: "Text for frame 1.2.3-2",
                img: "/assets/meme.png",
                imgAlt: "climate change doesn't matter ... if you stay indoors",
                next: "1.2.3-2.2"
            },
            "1.2.3-2.2": {
                title: "",
                text: ":?",
                img: "/assets/meme.png",
                imgAlt: "climate change doesn't matter ... if you stay indoors",
                next: "0"
            },
            "0": {
                title: "Ende.",
                text: "Zum Neustarten nach links.",
                img: "/assets/meme.png",
                imgAlt: "climate change doesn't matter ... if you stay indoors"
            }
        },
        obj: (params, route) => {
            const frameId = params.frameId;
            const frame = specialProjects["pictures"].pictureTree[frameId];

            return {
                frameId,
                frame
            };
        },
        check: (req, res) => {
            if (
                (!req.params.hasOwnProperty("frameId") || !req.params.frameId,
                !specialProjects["pictures"].pictureTree.hasOwnProperty(
                    req.params.frameId
                ))
            ) {
                res.redirect("/pictures/1");
                return false;
            }
            return true;
        }
    },
    nn: {
        render: __dirname + "/public/article.ejs",
        path: "/nn",
        contentPath: __dirname + "/projects/nn.html",
        title: "Klassifikation handgeschriebener Zahlen",
        authors: ["Noah"],
        shortContent:
            "Ein Neuronales Netzwerk in Python, welches handgeschriebene Zahlen erkennen kann.",
        content: "",
        obj: (params, route) => {
            return {
                route: {
                    ...route,
                    title: specialProjects["nn"].title
                },
                article: specialProjects["nn"]
            };
        }
    },
    "game-unity": {
        render: __dirname + "/public/article.ejs",
        path: "/game-unity",
        contentPath: __dirname + "/projects/gameUnity.html",
        title: "SheeehS",
        authors: ["Max", "Victor"],
        shortContent:
            "Ein scheinbar simples Spiel, das zur Meisterung jedoch viel Skill braucht. Die Idee wurde von Victor nach dem Pulsar weitergeführt und ist nun als App für Android & iOS verfügbar!",
        content: "",
        obj: (params, route) => {
            return {
                route: {
                    ...route,
                    title: specialProjects["game-unity"].title
                },
                article: specialProjects["game-unity"]
            };
        }
    },
    "game-sspo": {
        render: __dirname + "/public/article.ejs",
        path: "/game-sspo",
        contentPath: __dirname + "/projects/sspoGame.html",
        title: "Schere-Stein-Papier Online",
        authors: ["Milos", "Noah"],
        shortContent:
            "Schere-Stein-Papier mit Python geschrieben, aber Online!",
        content: "",
        obj: (params, route) => {
            return {
                route: {
                    ...route,
                    title: specialProjects["game-sspo"].title
                },
                article: specialProjects["game-sspo"]
            };
        }
    }
};

let firstTimeRead = true;

const readArticles = () => {
    for (let articleI in articles) {
        let article = articles[articleI];

        if (firstTimeRead) {
            article.id = articleI;
            article.path = "/article/" + article.path;
        }
        article.content = fs.readFileSync(article.contentPath, {
            encoding: "utf-8"
        });

        if (article.contentPath.endsWith(".md")) {
            article.content = article.content
                .substr(article.content.indexOf("---") + 3)
                .trim();
            article.content = marked(article.content);
        }
    }
};

const readSpecialProjects = () => {
    for (let projectI in specialProjects) {
        let project = specialProjects[projectI];

        if (firstTimeRead) {
            project.id = projectI;
        }
        if (project.hasOwnProperty("contentPath")) {
            project.content = fs.readFileSync(project.contentPath, {
                encoding: "utf-8"
            });
        }
    }
};

readArticles();
readSpecialProjects();
firstTimeRead = false;

setInterval(() => {
    readArticles();
    readSpecialProjects();
}, 1000); // 1000 * 60 * 60

const projects = [
    ...Object.values(articles),
    ...Object.values(specialProjects)
];

const routes = [
    {
        id: "index",
        path: "/",
        render: __dirname + "/public/index.ejs",
        inNav: true,
        title: "Home"
    },
    {
        id: "projects",
        path: "/projects",
        render: __dirname + "/public/projects.ejs",
        inNav: true,
        title: "Projekte",
        obj: () => {
            return {
                projects
            };
        }
    },
    {
        id: "about",
        path: "/about",
        render: __dirname + "/public/about.ejs",
        inNav: true,
        title: "Über Uns"
    },
    {
        id: "article",
        path: "/article/:articleId",
        render: __dirname + "/public/article.ejs",
        title: "Artikel",
        obj: (params, route) => {
            return {
                route: {
                    ...route,
                    title: articles[params.articleId].title
                },
                article: articles[params.articleId]
            };
        },
        check: (req, res) => {
            if (!articles.hasOwnProperty(req.params.articleId)) {
                res.redirect("/404");
                return false;
            }
            return true;
        }
    },
    {
        ...specialProjects["pictures"]
    },
    {
        ...specialProjects["nn"]
    },
    {
        ...specialProjects["game-unity"]
    },
    {
        ...specialProjects["game-sspo"]
    },
    {
        id: "404",
        path: "*",
        render: __dirname + "/public/404.ejs",
        title: "404",
        check: (req, res) => {
            res.status(404);
            return true;
        }
    }
];

app.get("/nn-run/:imgdata", (req, res) => {
    const imgdata = req.params.imgdata;
    if (imgdata.split(",").length === 784) {
        PythonShell.run(
            __dirname + "/python/nn/execute.py",
            {
                pythonPath: "python",
                args: [imgdata]
            },
            (err, output) => {
                if (err)
                    res.status(500).send({
                        error: "Something went wrong in execute.py!"
                    });

                res.send(output);
            }
        );
    } else {
        res.status(400).send({
            error: "Invalid image data!"
        });
    }
});

const sspoClient = cb => {
    const c = new PythonShell(__dirname + "/python/sspo/client.py", {
        pythonPath: "python",
        pythonOptions: ["-u"]
    });

    c.on("message", cb);

    return c;
};

let sspoClients = {};
app.get("/sspo-start", (req, res) => {
    const clientId = UUID();
    sspoClients[clientId] = {
        client: sspoClient(message => {
            // console.log("sspoClient >> ", clientId, message);

            sspoClients[clientId].messages.push(message);
        }),
        messages: [],
        lastActive: Date.now()
    };

    setTimeout(() => {
        sspoGet(clientId, res);
    }, 500);
});
app.get("/sspo-run/:clientId/:input", (req, res) => {
    const clientId = req.params.clientId;
    const input = req.params.input;

    if (sspoClients.hasOwnProperty(clientId)) {
        if (!sspoClients[clientId].client.terminated) {
            sspoClients[clientId].client.send(input);
            sspoClients[clientId].lastActive = Date.now();

            setTimeout(() => {
                sspoGet(clientId, res, {
                    input
                });
            }, 500);
        } else {
            res.send({
                error: "Client terminated!",
                clientId,
                exit: true
            });
        }
    } else {
        res.send({
            error: "Invalid client id!",
            clientId,
            exit: true
        });
    }
});
const sspoGet = (clientId, res, obj) => {
    if (sspoClients.hasOwnProperty(clientId)) {
        messages = sspoClients[clientId].messages;

        sspoClients[clientId].lastActive = Date.now();

        let exit = false;
        if (
            messages.indexOf("exit.") !== -1 ||
            sspoClients[clientId].client.terminated
        ) {
            // console.log("sspoClient exit >> ", clientId, messages);

            exit = true;

            sspoClients[clientId].client.end();
            sspoClients[clientId].client.terminate();

            delete sspoClients[clientId];
        }

        res.send({
            clientId,
            messages,
            ...obj,
            exit
        });

        if (!exit) {
            sspoClients[clientId].messages = [];
        }
    } else {
        res.send({
            error: "Invalid client id!",
            clientId,
            exit: true
        });
    }
};
app.get("/sspo-get/:clientId", (req, res) => {
    sspoGet(req.params.clientId, res);
});
const sspoCleanup = () => {
    const delTime = Date.now() - 15 * 1000;

    for (const clientId in sspoClients) {
        if (sspoClients[clientId].lastActive < delTime) {
            sspoClients[clientId].client.end();
            sspoClients[clientId].client.terminate();

            delete sspoClients[clientId];
        }
    }
};
setInterval(sspoCleanup, 15 * 1000);

// const mnist_arr = require(__dirname + "/python/mnist/mnist_test_arr.js");
// app.get("/nn-random", (req, res) => {
//     const randint1 = Math.floor(Math.random() * 10);
//     const randint2 = Math.floor(Math.random() * 100);

//     res.send({
//         label: randint1,
//         data: mnist_arr[randint1][randint2]
//     });
// });

for (let routeI = 0; routeI < routes.length; routeI++) {
    const route = routes[routeI];
    app.get(route.path, (req, res) => {
        if (route.hasOwnProperty("check")) {
            const check = route.check(req, res);
            if (!check) return false;
        }

        res.render(__dirname + "/public/layout.ejs", {
            routes,
            route,
            ...(route.hasOwnProperty("obj") ? route.obj(req.params, route) : {})
        });
    });
}

app.listen(3000, () => {
    console.log("App listening on port 3000!");
});

const sspoServer = new PythonShell(__dirname + "/python/sspo/server.py", {
    pythonPath: "python",
    pythonOptions: ["-u"]
});

// sspoServer.on("message", message => {
//     console.log("sspoServer >> ", message);
// });
