/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "bootstrap.min.css",
    "revision": "8fe70898895271ddc62823321011273a"
  },
  {
    "url": "bootstrap.min.js",
    "revision": "0a958254db529f99f475080fe2a6dcdb"
  },
  {
    "url": "favicon.ico",
    "revision": "fbfae4df7bcd28bbc365912946d2928c"
  },
  {
    "url": "jquery-3.3.1.slim.min.js",
    "revision": "a79e2167f5ab76ca6e4feeafb31fcc04"
  },
  {
    "url": "manifest.json",
    "revision": "c6b9096aa94b3ccae4ce4d5714cad200"
  },
  {
    "url": "popper.min.js",
    "revision": "07c3b4cdb94a0d798766707684e13ab2"
  },
  {
    "url": "favicons/x192.png",
    "revision": "c257fcdd1e5da4b3632069a912a89cf1"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

workbox.routing.registerRoute(/\.(?:ico|png|jpg|jpeg|svg)$/, new workbox.strategies.CacheFirst({ "cacheName":"images", plugins: [new workbox.expiration.Plugin({ maxEntries: 15, purgeOnQuotaError: false })] }), 'GET');
workbox.routing.registerRoute(/\.(?:js|json)$/, new workbox.strategies.CacheFirst({ "cacheName":"scripts", plugins: [new workbox.expiration.Plugin({ maxEntries: 15, maxAgeSeconds: 259200, purgeOnQuotaError: false })] }), 'GET');
workbox.routing.registerRoute(/\.css$/, new workbox.strategies.CacheFirst({ "cacheName":"styles", plugins: [new workbox.expiration.Plugin({ maxEntries: 15, maxAgeSeconds: 259200, purgeOnQuotaError: false })] }), 'GET');
workbox.routing.registerRoute(/(projects|about|404)?$/, new workbox.strategies.CacheFirst({ "cacheName":"pages", plugins: [new workbox.expiration.Plugin({ maxEntries: 15, maxAgeSeconds: 259200, purgeOnQuotaError: false })] }), 'GET');
workbox.routing.registerRoute(/article\/(.*?)$/, new workbox.strategies.CacheFirst({ "cacheName":"articles", plugins: [new workbox.expiration.Plugin({ maxEntries: 15, maxAgeSeconds: 259200, purgeOnQuotaError: false })] }), 'GET');
workbox.routing.registerRoute(/(nn|game-sspo|game-unity)$/, new workbox.strategies.NetworkOnly({ "cacheName":"interactives", plugins: [] }), 'GET');
