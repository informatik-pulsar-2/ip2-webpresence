module.exports = {
    globDirectory: "static/",
    globPatterns: ["*", "favicons/x192.png"],
    swDest: "static\\sw.js",
    runtimeCaching: [
        {
            urlPattern: /\.(?:ico|png|jpg|jpeg|svg)$/,
            handler: "CacheFirst",
            options: {
                cacheName: "images",
                expiration: {
                    maxEntries: 15
                }
            }
        },
        {
            urlPattern: /\.(?:js|json)$/,
            handler: "CacheFirst",
            options: {
                cacheName: "scripts",
                expiration: {
                    maxEntries: 15,
                    maxAgeSeconds: 60 * 60 * 24 * 3
                }
            }
        },
        {
            urlPattern: /\.css$/,
            handler: "CacheFirst",
            options: {
                cacheName: "styles",
                expiration: {
                    maxEntries: 15,
                    maxAgeSeconds: 60 * 60 * 24 * 3
                }
            }
        },
        {
            urlPattern: /(projects|about|404)?$/,
            handler: "CacheFirst",
            options: {
                cacheName: "pages",
                expiration: {
                    maxEntries: 15,
                    maxAgeSeconds: 60 * 60 * 24 * 3
                }
            }
        },
        {
            urlPattern: /article\/(.*?)$/,
            handler: "CacheFirst",
            options: {
                cacheName: "articles",
                expiration: {
                    maxEntries: 15,
                    maxAgeSeconds: 60 * 60 * 24 * 3
                }
            }
        },
        {
            urlPattern: /(nn|game-sspo|game-unity)$/,
            handler: "NetworkOnly",
            options: {
                cacheName: "interactives"
            }
        } /*,
        {
            urlPattern: /^(https:\/\/code\.jquery\.com\/jquery-3\.3\.1\.slim\.min\.js|https:\/\/stackpath\.bootstrapcdn\.com\/bootstrap\/4\.3\.1\/css\/bootstrap\.min\.css|https:\/\/cdnjs\.cloudflare\.com\/ajax\/libs\/popper\.js\/1\.14\.7\/umd\/popper\.min\.js|https:\/\/stackpath\.bootstrapcdn\.com\/bootstrap\/4\.3\.1\/js\/bootstrap\.min\.js|https:\/\/fonts\.googleapis\.com\/css\?family=Merriweather:300&display=swap|https:\/\/fonts\.gstatic\.com\/s\/merriweather\/(.*?)\/(.*?)\.woff2)$/,
            handler: "StaleWhileRevalidate",
            options: {
                cacheName: "external",
                expiration: {
                    maxEntries: 15
                }
            }
        }*/
    ]
};
